<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Provider;
use App\User;
use Auth;
use Socialite;


class AuthController extends Controller
{
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Redirect the user to the OAuth Provider.
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that 
     * redirect them to the authenticated users homepage.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();
        $authUser = $this->findOrCreateUser($user, $provider);
        Auth::login($authUser, true);
        return redirect($this->redirectTo);

        // return redirect()->to('/home');
    }

    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findOrCreateUser($user, $provider)
    {
        $account = Provider::whereProvider($provider)
            ->whereProviderId($user->id)
            ->first();

        if ($account) {

            return $account->user;

        } else {

            $account = new Provider([
                'provider_id' => $user->id,
                'provider' => $provider
            ]);

            $user_account = User::whereEmail($user->email)->first();

            if (!$user_account) {
                $user_account = User::create([
                    'email' => $user->email,
                    'name' => $user->name,
                    'password' => md5(rand(1,10000)),
                ]);
            }
            
            $account->user()->associate($user_account);
            $account->save();
    
            return $user_account;
        }        
    }
}